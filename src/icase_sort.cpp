/*
 * Copyright (c) 2018 Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include <vector>
#include <iostream>
#include <locale>
#include <fstream>
#include <algorithm>
#include "str.hpp"

#ifndef USE_BOOST
#define USE_BOOST 0
#endif

/// sorts a range of strings [first, last) into descending order ignoring the case of characters in accordance to the given locale.
///
/// requirements for R_iterator: the same as in std::sort
template <typename R_iterator>
void rsort_icase(R_iterator first, R_iterator last, const std::locale& loc); // definitions below.

std::locale get_locale(const char * name);



template <typename String, typename Char_tp, typename Traits>
std::vector<String> get_lines(std::basic_istream<Char_tp,Traits>& istr, Char_tp delim){
    //NRVO
    std::vector<String> lines;

    while(istr){
        Char_tp ch;
        String str;
        while (istr.get(ch) && ch!=delim) {
            str.push_back(ch);
        }
        if(!str.empty())
            lines.push_back(std::move(str));
    }
    return lines;
}

// defined below
template <typename T>
class Allocator;

template <typename Char_tp>
void perform(const char* input_path,const char* output_path, const std::locale& loc){

    using std::cerr;
    using std::endl;

    using String = task::Basic_string<Char_tp,std::char_traits<Char_tp>,Allocator<Char_tp>>;

    std::basic_ifstream<Char_tp> ifile(input_path);
    ifile.imbue(loc);
    if(!ifile.is_open()){
        std::cerr<<"can't open input file: "<<input_path<<endl;
        return;
    }
    std::basic_ofstream<Char_tp> ofile(output_path);
    ofile.imbue(loc);
    if(!ofile.is_open()){
        std::cerr<<"can't open output file: "<<output_path<<endl;
        return;
    }
    auto& ctype = std::use_facet<std::ctype<Char_tp>>(loc);
    std::vector<String> lines = get_lines<String>(ifile,ctype.widen('\n'));
    rsort_icase(lines.begin(),lines.end(),loc);
    for(auto&& line:lines){
        ofile<<line<<endl;
    }
}

int main(int argc, char *argv[])
{
    const char idefpath[]   = "input.txt";
    const char odefpath[]   = "output.txt";
    const char deflocale[]  = "";

    const char* ipath       = idefpath;
    const char* opath       = odefpath;
    const char* loc_str     = deflocale;

    using std::endl;

    if(argc==1){
        std::cout<<"usage: "<<argv[0]<<" output_file [input_file] [locale_name]"<<endl<<endl;
    }
    if(argc>1){
        opath   = argv[1];
    }
    if(argc>2){
        ipath   = argv[2];
    }
    if(argc>3){
        loc_str = argv[3];
    }

    std::cout << "trying to sort lines from \""     << ipath    << "\"" << endl <<
                 "locale to use is \""              << loc_str  << "\"" << endl <<
                 "output should be written to \""   << opath    << "\"" << endl <<
                 "boost::locale is used: "          << ((USE_BOOST) ? "yes" : "no" ) << endl;


    std::locale loc;
    try {
        loc = get_locale(loc_str);
    } catch (const std::runtime_error& err) {
        std::cerr<<"failed to construct locale named \""<<loc_str<<'"'<<endl;
        std::cerr<<"exception: "<<err.what()<<endl;
        return 1;
    }

    perform<char>(ipath,opath,loc);

    return 0;

}

#if USE_BOOST

#include <boost/locale.hpp>

template <typename R_iterator>
void rsort_icase(R_iterator first, R_iterator last, const std::locale& loc){
    using String        = typename std::iterator_traits<R_iterator>::value_type;
    using Char          = typename String::value_type;
    using Collator      = boost::locale::collator<Char>;
    using Collator_base = boost::locale::collator_base;

    auto& collator = std::use_facet<Collator>(loc);

    auto s_comp = [&collator] (const String& lhv, const String& rhv){
        int res = collator.compare(Collator_base::secondary,
                                   lhv.data(),lhv.data()+lhv.size(),
                                   rhv.data(),rhv.data()+rhv.size());
        return res>0;
    };
    std::sort(first,last,s_comp);
}

std::locale get_locale(const char * name){
    boost::locale::generator locgen;
    return std::locale(locgen.generate(name));
}

#else
template <typename R_iterator>
void rsort_icase(R_iterator first, R_iterator last, const std::locale& loc){
    using String    = typename std::iterator_traits<R_iterator>::value_type;
    using Char      = typename String::value_type;
    const std::ctype<Char>& ctype       = std::use_facet<std::ctype<Char>>(loc);
    const std::collate<Char>& collate   = std::use_facet<std::collate<Char>>(loc);

    // we need to copy anyway, lets pass by value
    auto s_comp = [&ctype,&collate] (String lhv, String rhv){
        /*
         * per Char collation combined with tolower / toupper + std::lexicographical_compare isn't
         * a correct way to compare if we care about a locale and utf-8. the overload of tolower that
         * accepts single character is senceless in the case of utf-8, and locale dependent collation
         * is context sensetive. we have to use std::collate facet and convert entire strings to the
         * same case. which one? std::regex_traits::translate_nocase uses translation to the lowercase.
         * anyway, this isn't the best solution, ctype can perform only 1 Char to 1 Char translations.
         * and this solution is totally useless if you are using MINGW without some "custom" locale,
         * since it has only "C" and "POSIX" locales!
        */

        ctype.tolower(lhv.data(), lhv.data()+lhv.size());
        ctype.tolower(rhv.data(), rhv.data()+rhv.size());

        return collate.compare(
                    lhv.data(),lhv.data()+lhv.size(),
                    rhv.data(),rhv.data()+rhv.size()
                    )>0;
    };
    std::sort(first,last,std::move(s_comp));
}

std::locale get_locale(const char * name){
    return std::locale(name);
}
#endif

// Are hand-written "operations on the heap" mandatory?
// if so, here they are. if not - this class is totally useless =)
template <typename T>
class Allocator{
public:
    using value_type    = T;
    using size_type     = std::size_t;
    using pointer       = value_type*;

    pointer allocate(size_type count){
        return static_cast<pointer>(::operator new(count*sizeof(T)));
    }
    void deallocate(pointer p, size_type count) noexcept{
        (void)count;// unused
        ::operator delete(p);
    }

    Allocator()=default;

    template<typename U>
    Allocator(const Allocator<U>&): Allocator(){};

    // actually the following is optional, allocator_traits provides it
    template<typename U, typename ... Args>
    void construct(U* ptr,Args&& ... args){
        new (static_cast<void*>(ptr)) U(std::forward<Args>(args)...);
    }
    template<typename U>
    void destroy(U* ptr){
        ptr->~U();
    }
};

template<typename T,typename U>
inline bool operator==(const Allocator<T>&,const Allocator<U>&){return true;}
template<typename T,typename U>
inline bool operator!=(const Allocator<T>&,const Allocator<U>&){return false;}
