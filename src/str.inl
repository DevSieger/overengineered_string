/*
 * Copyright (c) 2018 Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <utility>
#include <type_traits>
#include <memory>
#include <cassert>

namespace task {

// details
namespace dtl {

// may Allocator be final? if so we need that.
/*
 * helper class to enable empty base optimization.
 *
 * to use it multiple times as a base for the single class
 * an additional index parameter required since parameter types may be equal.
 */
template<typename T, bool=std::is_empty<T>::value && !std::is_final<T>::value>
class EBO_helper;

template <typename T>
class EBO_helper<T,true>: T{
public:
    using Value = T;
    EBO_helper() = default;
    EBO_helper(const Value& t)      noexcept(std::is_nothrow_copy_constructible<Value>::value):
        Value(t){}
    EBO_helper(Value&& t)           noexcept(std::is_nothrow_move_constructible<Value>::value):
        Value(std::move(t)){}

    Value& get()&                noexcept {return *this;}
    const Value& get()const &    noexcept {return *this;}
    Value&& get()&&              noexcept {return std::move(*this);}

private:
};

template <typename T>
class EBO_helper<T,false>{
public:
    using Value = T;

    EBO_helper() = default;
    EBO_helper(const Value& t)      noexcept(std::is_nothrow_copy_constructible<Value>::value):
        m_(t){}
    EBO_helper(Value&& t)           noexcept(std::is_nothrow_move_constructible<Value>::value):
        m_(std::move(t)){}

    Value& get()&               noexcept    {return m_;}
    const Value& get()const &   noexcept    {return m_;}
    Value&& get()&&             noexcept    {return std::move(m_);}
private:
    Value m_;
};

// void_t emulation, no C++17
template <typename ... Tps>
struct Void_t{
    using type=void;
};

/*
 * there was no allocator_traits::is_always_equal before c++17,
 * but some implementations (look at gcc) uses it even if __cplusplus <= 201402,
 * so I think it is safe to emulate it.
 */
template<typename Alloc_tp,typename T  = void>
struct Alloc_is_always_equal_emulation: std::is_empty<Alloc_tp>::type {};

// doesn't handle the case then is_always_equal is non-type member, it
// is possible to handle, but std::allocator_traits does the same.
template<typename Alloc_tp>
struct Alloc_is_always_equal_emulation<Alloc_tp, typename Void_t<typename Alloc_tp::is_always_equal>::type>:
        Alloc_tp::is_always_equal
{
    static_assert(
            std::is_base_of<std::true_type,typename Alloc_tp::is_always_equal>::value ||
            std::is_base_of<std::false_type,typename Alloc_tp::is_always_equal>::value,
            "Alloc_tp::is_always_equal should be derived from or equal to std::true_type or std::false_type");
};

// the fallback case
template<typename Alloc_tp,typename T  = void>
struct Alloc_is_always_equal: Alloc_is_always_equal_emulation<Alloc_tp>{};

// use std::allocator_traits<Alloc_tp>::is_always_equal if available
template<typename Alloc_tp>
struct Alloc_is_always_equal<Alloc_tp,
        typename Void_t< typename std::allocator_traits<Alloc_tp>::is_always_equal>::type>:
        std::allocator_traits<Alloc_tp>::is_always_equal {
};

// is_nothrow_swappable emulation, if no C++17
#if __cplusplus > 201402ul
template <typename T>
using Is_nothrow_swappable = std::is_nothrow_swappable<T>;
#else

namespace Is_nothrow_swappable_ns {

using std::swap;

template <typename T,typename = std::true_type>
struct Hlpr: std::false_type {};

template <typename T>
struct Hlpr<T,std::integral_constant<bool,
        noexcept (swap(std::declval<T&>(),std::declval<T&>()))>>: std::true_type {};

}

template <typename T>
using Is_nothrow_swappable = Is_nothrow_swappable_ns::Hlpr<T>;
#endif

} // namespace dtl

namespace policy{

template<typename Char_tp,typename Char_traits_tp, typename Allocator_tp, std::size_t sso_data_size_vp>
class SSO_storage_policy{
public:
    // standard names, from std::string, have to be lowercase
    using value_type        = Char_tp;

    // reason: char_traits doesn't provide a way to construct / destruct characters
    static_assert( std::is_trivially_default_constructible<Char_tp>::value &&
                   std::is_trivially_destructible<Char_tp>::value,
                   "Char_tp should have trivial default constructor & trivial destructor");

    using traits_type       = Char_traits_tp;
    // AFAIK the right way to get an allocator type
    using allocator_type    = typename std::allocator_traits<Allocator_tp>::template rebind_alloc<value_type>;
    using size_type         = typename std::allocator_traits<allocator_type>::size_type;

    static constexpr size_type small_string_threshold(){
        // 8 bytes on x86 is impractical, only 7 bytes for small strings.
        // 16 is seamless, but the best way is to move it to a template parameter.
        const unsigned block_size = sizeof(Long_data) > sso_data_size_vp ? sizeof(Long_data):sso_data_size_vp;
        const unsigned max_size = (block_size-1) / sizeof(Char_tp);
        // we need null terminated strings, -1
        return (max_size>1)? max_size-1:0;
    }

    size_type size()const noexcept{
        return small()?u_.small_.size : u_.long_.size;
    }
    size_type capacity()const noexcept{
        return small()?small_string_threshold():u_.long_.capacity;
    }

    /*
     * String class uses SSO_storage_policy(const SSO_storage_policy& other, const allocator_type& alloc)
     * and passes allocator selected from allocator_traits, to avoid code dublication.
     */
    SSO_storage_policy(const SSO_storage_policy& other) = delete;

    size_type max_size() const noexcept{
        return Alloc_traits::max_size(allocator())-1;
    }

protected:
    using Alloc_traits = std::allocator_traits<allocator_type>;

    /// returns true if initialization of an empty string is nothrow.
    ///
    /// make sure that std::pointer_traits<Pointer>::pointer_to(obj) is nothrow!
    static constexpr bool is_init_noexcept(){
        /*
         * AFAIK the standard requires constructors, swap, move, copy and comparisson
         * operations of the pointer types that are defined in allocator_type have to be nothrow.
         * but I'm not sure about std::pointer_traits::pointer_to (it is used in local_str()).
         * also the standard requires traits_type functions to be nothrow.
        */
        return noexcept(std::pointer_traits<Pointer>::pointer_to(std::declval<value_type&>()));
    }

    static constexpr bool is_move_assignment_noexcept(){
        // Alloc_traits::deallocate should not throw, this is standard requirement.
        // c++17 require move assignment of the allocator to be nothrow in
        // that case, but for c++14 we have to check.
        const bool propagate_case =
                Alloc_traits::propagate_on_container_move_assignment::value &&
                std::is_nothrow_move_assignable<allocator_type>::value;

        // if allocators is equal we can move. In the other case we have to copy, copy
        // assignment can throw because of allocations.
        const bool no_propagate_case =
                !Alloc_traits::propagate_on_container_move_assignment::value &&
                Alloc_is_always_eq::value;

        return (propagate_case || no_propagate_case) && is_init_noexcept();
    }


    static constexpr bool is_swap_noexcept(){
        // if allocator propagation is nothrow we can swap safely. c++17 require allocator swap
        // to be nothrow in that case but for c++14 we have to check
        const bool propagate_case =
                Alloc_traits::propagate_on_container_swap::value &&
                dtl::Is_nothrow_swappable<allocator_type>::value;

        const bool no_propagate_case =
                !Alloc_traits::propagate_on_container_swap::value && (
                // if allocators is always equal we can swap safely
                Alloc_is_always_eq::value ||
                // swap with non equal allocators is UB
                // but fallback to move-assignment swap may be nothrow:
                is_move_assignment_noexcept() );

        return (propagate_case || no_propagate_case) && is_init_noexcept();
    }

    SSO_storage_policy() noexcept(is_init_noexcept() && std::is_nothrow_default_constructible<allocator_type>::value){
        init_invalid();
    }

    // the standard requires allocator_type to be nothrow copy constructable.
    explicit SSO_storage_policy(const allocator_type& alloc) noexcept(is_init_noexcept())
        :u_(alloc)
    {
        init_invalid();
    }

    SSO_storage_policy(const SSO_storage_policy& other, const allocator_type& alloc):
        u_(alloc)
    {
        if(other.small()){
            str_ = local_str();
            copy_local_data(other.u_.small_);
        }else{
            u_.long_.size       = other.size();
            u_.long_.capacity   = u_.long_.size;
            Pointer p = allocate(u_.long_.capacity);
            try {
                copy_entire(raw(p),raw(other.str_),u_.long_.size);
            } catch (...) {
                deallocate(p,u_.long_.capacity);
            }
            str_ = std::move(p);
        }
    }

    // the standard requires allocator_type to be nothrow move constructable.
    SSO_storage_policy(SSO_storage_policy&& other) noexcept(is_init_noexcept())
        : u_(std::move(other.allocator()))
    {
        if(other.small()){
            str_ = local_str();
            copy_local_data(other.u_.small_);
        }else{
            u_.long_    = std::move(other.u_.long_);
            str_        = std::move(other.str_);
        }
        other.init_invalid();
    }

    SSO_storage_policy& operator=(SSO_storage_policy&& other) noexcept(is_move_assignment_noexcept()) {
        if(this == &other) return *this;
        assign(std::move(other),typename Alloc_traits::propagate_on_container_move_assignment());
        return *this;
    }

    SSO_storage_policy& operator=(const SSO_storage_policy& other){
        if(this == &other) return *this;
        assign(other,typename Alloc_traits::propagate_on_container_copy_assignment());
        return *this;
    }

    // required to have constant complexity
    void swap(SSO_storage_policy &other) noexcept(is_swap_noexcept()){
        if(this == &other) return;
        swap(other,typename Alloc_traits::propagate_on_container_swap());
    }

    ~SSO_storage_policy(){
        if(!small()){
            deallocate(str_,u_.long_.capacity);
        }
        //stays invalid
    }

    const allocator_type&   allocator()const&   { return u_.Alloc_base::get();}
    allocator_type&         allocator()&        { return u_.Alloc_base::get();}
    allocator_type&&        allocator()&&       { return std::move(u_).Alloc_base::get();}

    // calling this with new_size > capacity() is ub
    void set_size(size_type new_size) noexcept {
        assert(new_size<=capacity());
        if(small()){
            u_.small_.size=new_size;
        }else{
            u_.long_.size = new_size;
        }
    }

    // calling this with new_capacity < size() is ub
    void set_capacity(size_type new_capacity);

            value_type* get_data()          { return raw(str_);}
    const   value_type* get_data() const    { return raw(str_);}

private:

    using Pointer   = typename Alloc_traits::pointer;
    using Alloc_is_always_eq = dtl::Alloc_is_always_equal<allocator_type>;

    /// deallocates "dynamic" storage if any, and resets the string to valid state
    ///
    /// no effect is small()==true
    void deallocate() noexcept(is_init_noexcept()){
        if(!small()){
            // this one isn't permited to throw
            deallocate(str_,u_.long_.capacity);
            u_.long_.capacity = 0;
            init_invalid();
        }
    }

    /// wraps allocation to ensure that the null character is counted
    Pointer allocate(size_type capacity_a){
        return Alloc_traits::allocate(allocator(),capacity_a+1);
    }

    /// wraps deallocation to ensure that the null character is counted
    void deallocate(Pointer p, size_type capacity_a){
        Alloc_traits::deallocate(allocator(),p,capacity_a+1);
    }

    /// wraps coping to ensure that the null character is counted
    ///
    /// this one to copy entire string with the terminating null character
    void copy_entire(value_type * dst, const value_type* src, size_type length){
        // user provided operation, may it throw an exception?
        traits_type::copy(dst,src,length+1);
    }

    // propogates an allocator.
    // this is template to use forward reference.
    // forward reference here to avoid code dublication.
    template<typename SSO_storage_tp>
    std::enable_if_t<std::is_same<std::decay_t<SSO_storage_tp>,SSO_storage_policy>::value>
    assign(SSO_storage_tp && other, std::true_type) {
        if(!small() && !Alloc_is_always_eq::value && allocator()!=other.allocator()){
            deallocate();
        }
        allocator() = std::forward<SSO_storage_tp>(other).allocator();
        assign(std::forward<SSO_storage_tp>(other),std::false_type());
    }

    void swap(SSO_storage_policy &other, std::true_type){
        using std::swap;
        swap(allocator(),other.allocator());
        //that will handle the rest
        swap_compatible(other);
    }

    //no propagation
    void assign(SSO_storage_policy&& other, std::false_type){
        if(other.small()){
            if(!small()){
                deallocate(str_,u_.long_.capacity);
            }
            str_ = local_str();
            copy_local_data(other.u_.small_);
        }else{
            if(Alloc_is_always_eq::value || allocator()==other.allocator()){
                if(!small()){
                    deallocate(str_,u_.long_.capacity);
                }
                str_        = std::move(other.str_);
                // move is not-necessary
                u_.long_    = std::move(other.u_.long_);
                other.init_invalid();
            }else{
                //need to copy
                assign(other,std::false_type{});
            }
        }
    }

    // basic copying, throws
    void assign(const SSO_storage_policy& other, std::false_type){
        size_type sz = other.size();
        if(sz>capacity()){
            set_size(0);
            set_capacity(sz);
        }
        set_size(sz);
        copy_entire(raw(str_),raw(other.str_),sz);
    }

    void swap(SSO_storage_policy &other, std::false_type){
        // this is UB to swap allocator aware containers if their allocators doesn't equal
        // we are free from complexity requirements, lets use move.
        if(!Alloc_is_always_eq::value && allocator()!=other.allocator() && !(small() && other.small())){
            SSO_storage_policy tmp = std::move(other);
            other = std::move(*this);
            *this = std::move(tmp);
            return;
        }else{
            swap_compatible(other);
        }
    }

    // the case of equal or propagated allocators
    void swap_compatible(SSO_storage_policy &other){
        if( small() != other.small() ){
            SSO_storage_policy& sstr    = small() ? *this : other;
            SSO_storage_policy& lstr    = small() ? other : *this;

            sstr.str_       = std::move(lstr.str_);
            lstr.str_       = lstr.local_str();
            Long_data tmp   = lstr.u_.long_;
            lstr.copy_local_data(sstr.u_.small_);
            sstr.u_.long_   = tmp;
        }else{
            if(small()){
                Small_data tmp;
                copy_local_data(tmp,other.u_.small_);
                copy_local_data(other.u_.small_,u_.small_);
                copy_local_data(u_.small_,tmp);
            }else{
                using std::swap;
                swap(str_,other.str_);
                swap(u_.long_,other.u_.long_);
            }
        }
    }

    void init_invalid() noexcept(is_init_noexcept()){
        str_ = local_str();
        u_.small_.size  = 0;
        traits_type::assign(str_[0],value_type{});
    }

    // two POD types to represent long and short strings
    // bugs can happen but construction and allocation should be handled manually.
    struct Long_data{
        size_type size;
        size_type capacity;
    };
    static_assert ( std::is_pod<Long_data>::value,
                    "Long_data should be POD type, allocator_type doesn't meet Allocator requirements?" );

    struct Small_data{
        using Size = std::conditional_t< sso_data_size_vp < 256, unsigned char, size_type>;
        value_type data[small_string_threshold()+1];
        Size size;
    };

    /*
     * to be safe that we don't have to call contructors / destructors when switching
     * union members because of some code changes. value_type is trivially default
     * constructible / destructible so it should be true.
    */
    static_assert ( std::is_trivially_default_constructible<Small_data>::value &&
                    std::is_trivially_destructible<Small_data>::value,
                    "value_type isn't trivial enought" );

    using Alloc_base = dtl::EBO_helper<Allocator_tp>;

    class Data_pack: public Alloc_base{
    public:
        using Alloc_base::Alloc_base;
        // small() is the indicator of currently active object
        union{
            Small_data small_;
            Long_data long_;
        };
    };

    // we have to use traits to copy/move notwithstanding if Small_data is POD
    void copy_local_data(const Small_data& other) noexcept {
        assert(small());
        copy_local_data(u_.small_,other);
    }

    // for swap
    static void copy_local_data(Small_data& dest, const Small_data& src) noexcept {
        dest.size = src.size;
        if(src.size>0){
            traits_type::copy(dest.data,src.data,src.size+1);
        }else{
            traits_type::assign(dest.data[0],value_type{});
        }
    }

     /*
      * to obtain raw poiner from Pointer
      * ptr considered not null.
      * ptr usually comes from Alloc_traits::allocate and local_str(), that returns dereferenceable pointers
      * ptr considered valid and not throwing on dereference.
      */
    value_type* raw(const Pointer& ptr) noexcept{
        assert(ptr);
        return std::addressof(*ptr);
    }
    const value_type* raw(const Pointer& ptr)const noexcept{
        assert(ptr);
        return std::addressof(*ptr);
    }

    Pointer local_str() noexcept(is_init_noexcept()){
        return std::pointer_traits<Pointer>::pointer_to(*reinterpret_cast<value_type*>(std::addressof(u_)));
    }

    bool small()const noexcept{
        return static_cast<const void*>(raw(str_))==static_cast<const void*>(&u_);
    }

    Pointer str_;
    Data_pack u_;
};

template<typename Char_tp, typename Char_traits_tp, typename Allocator_tp, std::size_t sso_data_size_vp>
void SSO_storage_policy<Char_tp,Char_traits_tp,Allocator_tp,sso_data_size_vp>::
set_capacity(SSO_storage_policy::size_type new_capacity){
    assert(new_capacity>=size());

    if(new_capacity==0){
        deallocate();
        return;
    }
    // TODO: optimize size()==0 case
    if(new_capacity<=small_string_threshold()){
        if(small()) return;
        value_type *src = raw(str_);
        size_type cap   = u_.long_.capacity;
        size_type sz = u_.long_.size;
        //switch to small
        value_type *dst = u_.small_.data;
        u_.small_.size = sz;

        try {
            copy_entire(dst,src,sz);
        } catch (...) {
            // restore the state, union was written
            u_.long_.size = sz;
            u_.long_.capacity = cap;
            throw;
        }
        // moved
        Pointer ptr = local_str();
        using std::swap;
        swap(str_,ptr);
        deallocate(ptr,cap);
    }else{
        if(small()){
            Pointer ptr = allocate(new_capacity);
            value_type *dst = raw(ptr);
            std::size_t sz = u_.small_.size;
            value_type *src = u_.small_.data;
            try {
                copy_entire(dst,src,sz);
            } catch (...) {
                deallocate(ptr,new_capacity);
                throw;
            }
            // moved
            //switch to long
            str_ = std::move(ptr);
            u_.long_.size =  sz;
            u_.long_.capacity = new_capacity;
        }else{
            Pointer ptr = allocate(new_capacity);
            try {
                copy_entire(raw(ptr),raw(str_),u_.long_.size);
            } catch (...) {
                deallocate(ptr,new_capacity);
                throw;
            }

            auto old_allocated = u_.long_.capacity;

            using std::swap;
            swap(str_,ptr);
            //size is unchanged
            u_.long_.capacity = new_capacity;
            deallocate(ptr,old_allocated);
        }
    }
}
}
} //  namespace task
