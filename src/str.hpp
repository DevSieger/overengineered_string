/*
 * Copyright (c) 2018 Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <utility>
#include <type_traits>
#include <cassert>
#include <ostream>

// required for std::allocator
#include <memory>

// note to Targem:
// this one from dk, my own little library. I don't want to use it in the
// "right way", just a stand alone file, to make things easer.
#include "comparable.hpp"

namespace task {

/*
 * Note: std::basic_string uses char_traits to _write_ elements and doesn't
 * use allocator_traits::create to create them. when you use your own class
 * with std::basic_string it doesn't call constructors and destructor of
 * that class almost at all. it calls only the default constructor to create
 * the null character and passes non-initialized objects to functions of
 * char_traits. is it possible to construct / destroy objects of non-trivial
 * class using char_traits? I guess no, use std::vector instead. this is why
 * std::string isn't a real container. IMO string classes should explicitly
 * require character type to be trivial.
*/
namespace policy{

/*!
 * \brief Dummy class for the documentation purpose, describes implementation
 * requirements for a storage policy.
 *
 * \tparam Char_tp          a char-like type
 * \tparam Char_traits_tp   a character traits class
 * \tparam Allocator_tp     an allocator class
 *
 * Do not use that class, this will produce link errors.
 *
 * The data to hold is null-terminated strings. The terminating null-character
 * is exluded from values of capacity and size methods, but should be preserved
 * and allocated (i.e. policy allocates space for one more charater, and copies
 * one more character on reallocations). The only case where the terminating
 * null-character is set by policy is producing an object holding the empty
 * string (including construction cases and possible optimizations of swap or
 * assign to avoid copying). The null-character is a value initialized one (it
 * means Char_tp{}).
 *
 * The allocator should be handled with the respect of std::allocator_traits.
 * Data copying / moving and asignment of the null-character should be handled
 * using Char_traits_tp. Char_traits_tp and Allocator_tp are considered
 * satisfying the requirements of the standard.
 *
 * The real implementation of policy declares move and swap operations noexcept
 * whenever it is correct.
*/
template< typename Char_tp, typename Char_traits_tp, typename Allocator_tp>
struct Dummy_storage_policy{

    /// an element type
    using value_type        = Char_tp;

    using allocator_type    = typename std::allocator_traits<Allocator_tp>::template rebind_alloc<value_type>;
    using size_type         = typename std::allocator_traits<allocator_type>::size_type;

    /// constructs an object holding the empty string
    Dummy_storage_policy() = default;

    /// constructs an object holding the empty string
    Dummy_storage_policy(const allocator_type& alloc);

    Dummy_storage_policy(const Dummy_storage_policy& other, const allocator_type& alloc);
    Dummy_storage_policy(const Dummy_storage_policy& other) = delete;
    Dummy_storage_policy(Dummy_storage_policy&&)            = default;

    Dummy_storage_policy& operator=(Dummy_storage_policy&& other)       = default;
    Dummy_storage_policy& operator=(const Dummy_storage_policy& other)  = default;

    // requirements are the same as for the allocator aware containers from the standard:
    // * complexity requirement is O(1)
    // * UB if allocator traits doesn't declare allocator propagation on swap and allocators doesn't equal
    void swap(Dummy_storage_policy &other);

    /// returns the data size (in elements)
    size_type size()const noexcept;
    /// returns the size of the available storage (in elements)
    size_type capacity()const noexcept;
    /// the largest possible size of the available storage
    size_type max_size() const noexcept;

    /// returns a reference to the allocator
    const   allocator_type& allocator() const&;

    /// returns the pointer to the stored data.
            value_type*     get_data();
    const   value_type*     get_data() const;

    /// sets the size of the stored data (in elements)
    ///
    /// UB if new_size > capacity(). Doesn't set the terminating null-character.
    /// Provides the strong exception guaranty.
    void set_size(size_type new_size) noexcept;

    /// changes the size of the available storage(in elements),
    ///
    /// Provides the strong exception guaranty. UB if new_capacity < size().
    void set_capacity(size_type new_capacity);
};

/*!
 * \brief Short string optimization storage policy
 *
 * \tparam sso_data_size_vp minimal size in bytes for local storage.
 *
 * Local storage contains not only string content, but also the size of the string (1 byte if sso_data_size_vp < 256)
 * To use this policy with Basic_string a helper template SSO_storage_policy_adaptor is provided.
 */
template<typename Char_tp,typename Char_traits_tp, typename Allocator_tp, std::size_t sso_data_size_vp = 16>
class SSO_storage_policy;

// to use with Basic_string pass SSO_storage_policy_adaptor<N>::policy as policy
template<std::size_t size_vp>
struct SSO_storage_policy_adaptor{
    template<typename Char_tp,typename Char_traits_tp, typename Allocator_tp>
    using policy = SSO_storage_policy<Char_tp,Char_traits_tp,Allocator_tp,size_vp>;
};

// recommended for single byte characters.
// not so big, minimal size to make local storage size the same for both 32 bit and 64 bit builds
template<typename Char_tp,typename Char_traits_tp, typename Allocator_tp>
using SSO_storage_policy_16 = SSO_storage_policy_adaptor<16>::policy<Char_tp,Char_traits_tp,Allocator_tp>;

}

/*!
 * \brief the string class
 * \tparam Char_tp              a type of characters
 * \tparam Char_traits_tp       a character traits class
 * \tparam Allocator_tp         an allocator class
 * \tparam Storage_policy_tp    the policy class that handles data storage
 *
 * This class has been written to be close to std::string in method names and
 * behaviour (why not?). Only a subset of std::string methods is implemented,
 * because the task doesn't require it all. The documentation for methods is
 * ommited, to see what methods do look at std::string documentation.
 *
 * It is overengineered for the task because I wanted to showcase
 * my skill (or its absence =D ) and ... just for fun.
*/
template<
        typename Char_tp,
        typename Char_traits_tp = std::char_traits<Char_tp>,
        typename Allocator_tp = std::allocator<Char_tp>,
        template<typename,typename,typename> class Storage_policy_tp=policy::SSO_storage_policy_16
        >
class Basic_string:
        // policies is often used with private inheritance,
        // but in the original Alexandrescu book he uses public one.
        // doesn't really matter here.
        public Storage_policy_tp<Char_tp,Char_traits_tp,Allocator_tp>,
        /*
         * I don't want to write comparison operations yet another time.
         * dk::Comparable was written by me. If you doubt - just look at
         * another parts of the code :)
        */
        public dk::Comparable<
            Basic_string<Char_tp,Char_traits_tp,Allocator_tp,Storage_policy_tp>,
            Basic_string<Char_tp,Char_traits_tp,Allocator_tp,Storage_policy_tp>,
            const Char_tp*
        >
{
    using Storage = Storage_policy_tp<Char_tp,Char_traits_tp,Allocator_tp>;

    static constexpr bool is_swap_noexcept(){
        return noexcept(std::declval<Basic_string&>().Storage::swap(std::declval<Basic_string&>()));
    }

public:
    using traits_type       = Char_traits_tp;
    using value_type        = Char_tp;

    static_assert(std::is_same<value_type,typename traits_type::char_type>::value,
                  "Char_traits_tp::char_type isn't Char_tp");

    // there is highlighting problems in IDE when
    // using typename Storage::allocator_type;
    using allocator_type    = typename Storage::allocator_type;
    using size_type         = typename Storage::size_type;

    // basic implementation of iterators
    using iterator                  = value_type*;
    using const_iterator            = const value_type*;

    Basic_string() = default;

    explicit Basic_string(const allocator_type& alloc):
        Storage(alloc)
    {}

    Basic_string(const Basic_string& str, const allocator_type& alloc):
        Storage(str,alloc)
    {}

    Basic_string(const Basic_string& str):
        Storage (str,Alloc_traits::select_on_container_copy_construction(str.Storage::allocator()))
    {}

    Basic_string(Basic_string&& other)              = default;
    Basic_string& operator=(const Basic_string&)    = default;
    Basic_string& operator=(Basic_string&&)         = default;

    ~Basic_string() = default;

    void swap(Basic_string& other) noexcept(is_swap_noexcept()) {
        if(this == &other) return;
        Storage::swap(other);
    }

    // it's ok to not introduce that in the namespace scope, this is only for ADL.
    friend void swap(Basic_string& lhv, Basic_string& rhv) noexcept(is_swap_noexcept()) {
        lhv.swap(rhv);
    }

    Basic_string(const value_type* cstr, size_type sz, const allocator_type& alloc = allocator_type()):
        Storage(alloc)
    {
        if(!cstr) return;
        // default constucted Storage have to be valid empty string
        if(sz>0){
            capacity(sz);
            traits_type::copy(get_data(),cstr,sz);
            size(sz);
        }
    }

    Basic_string(const value_type* cstr, const allocator_type& alloc = allocator_type()):
        Basic_string(cstr,traits_type::length(cstr),alloc)
    {}

    Basic_string(size_type count, value_type ch, const allocator_type& alloc = allocator_type()):
        Storage(alloc)
    {
        if(!count) return;
        resize(count,ch);
    }

    template<typename Input_iterator>
    Basic_string(Input_iterator first, Input_iterator last,
                 std::enable_if_t<
                    !std::is_integral<Input_iterator>::value, const allocator_type&
                 > alloc = allocator_type()):
        Storage(alloc)
    {
        if(first==last) return;
        fill_with(first,last,typename std::iterator_traits<Input_iterator>::iterator_category());
    }

    Basic_string(const_iterator first, const_iterator last, const allocator_type& alloc = allocator_type()):
        Basic_string(first,last-first,alloc)
    {}

    Basic_string(iterator first, iterator last, const allocator_type& alloc = allocator_type()):
        Basic_string(first,last-first,alloc)
    {}

    iterator        begin()         noexcept { return get_data();}
    const_iterator  begin()  const  noexcept { return get_data();}
    const_iterator  cbegin() const  noexcept { return get_data();}
    iterator        end()           noexcept { return get_data()+size();}
    const_iterator  end()    const  noexcept { return get_data()+size();}
    const_iterator  cend()   const  noexcept { return get_data()+size();}

    value_type& operator[](size_type ix){
        assert(ix<=size());
        return get_data()[ix];
    }
    const value_type& operator[](size_type ix)const{
        assert(ix<=size());
        return get_data()[ix];
    }

    int compare(size_type pos, size_type count, const value_type* str, size_type str_size) const{
        validate_range(pos,count);
        size_type ms = (count<=str_size)?count:str_size;
        int res = traits_type::compare(get_data()+pos,str,ms);
        if(!res){
            return (count<str_size)?-1:(count==str_size?0:1);
        }
        return res;
    }
    int compare(const Basic_string& other)  const noexcept{
        return compare(0,size() ,other.get_data(),other.size());
    }
    int compare(const value_type* cstr)     const noexcept{
        return compare(0,size() ,cstr,traits_type::length(cstr));
    }

    friend bool operator == (const Basic_string& lhv, const Basic_string& rhv){
        // compare is slower
        if(lhv.size()!=rhv.size()) return false;
        return !traits_type::compare(lhv.get_data(),rhv.get_data(),lhv.size());
    }
    // this one inside to enable conversions
    friend bool operator < (const Basic_string& lhv, const Basic_string& rhv){
        return lhv.compare(rhv)<0;
    }

    // behaviour close to std::string, but not the same.
    // Storage allocated only once.
    // reason to be the friend - uses Storage::allocator().
    friend Basic_string operator+(const Basic_string& lhv, const Basic_string& rhv){
        // NRVO
        Basic_string res(Alloc_traits::select_on_container_copy_construction(lhv.Storage::allocator()));
        res.reserve(lhv.size()+rhv.size());
        res.append(lhv).append(rhv);
        return res;
    }

    // the next overloads of operator+ are friends to enable possible implicit conversions.
    friend Basic_string operator+(const Basic_string& lhv, Basic_string&& rhv){
        return std::move(rhv.insert(0,lhv));
    }
    friend Basic_string operator+(Basic_string&& lhv, const Basic_string& rhv){
        return std::move(lhv.append(rhv));
    }

    // this one is necessary,
    // without it "Basic_string{}+Basic_string{}" is ambigious
    friend Basic_string operator+(Basic_string&& lhv, Basic_string&& rhv){
        return std::move(lhv.append(rhv));
    }

    size_type max_size()    const noexcept{
        return Storage::max_size();
    }
    size_type size()        const noexcept{
        return Storage::size();
    }
    size_type capacity()    const noexcept{
        return Storage::capacity();
    }

    void reserve(size_type new_capacity){
        if(new_capacity<size()){
            new_capacity = size();
        }
        capacity(new_capacity);
    }

    void resize(size_type new_size, value_type ch = value_type()){
        const size_type sz = size();
        if(new_size == sz) return;
        if(new_size > sz){
            if(new_size > capacity()){
                capacity(new_size);
            }
            traits_type::assign(get_data()+sz,new_size-sz,ch);
        }
        size(new_size);
    }

    bool empty()const noexcept{
        return !size();
    }

    allocator_type get_allocator()const{
        return Storage::allocator();
    }

    // all other string modifications may be done using replace.
    Basic_string& replace(size_type pos, size_type count, const value_type* cstr, size_type str_size){
        validate_range(pos,count);
        shift(pos+count,pos+str_size);
        if(str_size>1){
            traits_type::copy(get_data()+pos,cstr,str_size);
        }else if(str_size==1){
            traits_type::assign(get_data()[pos],*cstr);
        }
        return *this;
    }
    Basic_string& replace(const_iterator first, const_iterator last, const value_type* cstr, size_type str_size){
        return replace(first-begin(),last-first,cstr,str_size);
    }

    Basic_string& replace(size_type pos, size_type rep_size, size_type ch_count,value_type ch){
        validate_range(pos,rep_size);
        shift(pos+rep_size,pos+ch_count);

        if(ch_count>1){
            traits_type::assign(get_data()+pos,ch_count,ch);
        }else if(ch_count==1){
            traits_type::assign(get_data()[pos],ch);
        }
        return *this;
    }
    Basic_string& replace(const_iterator first, const_iterator last,
                          size_type ch_count,value_type ch) {
        return replace(first-begin(),last-first,ch_count,ch);
    }

    Basic_string& replace(size_type pos, size_type count, const value_type* cstr) {
        return replace(pos,count,cstr,traits_type::length(cstr));
    }
    Basic_string& replace(const_iterator first, const_iterator last, const value_type* cstr) {
        return replace(first,last,cstr,traits_type::length(cstr));
    }

    Basic_string& replace(size_type pos, size_type count, const Basic_string& str) {
        return replace(pos,count,str.get_data(),str.size());
    }
    Basic_string& replace(const_iterator first, const_iterator last, const Basic_string& str) {
        return replace(first,last,str.get_data(),str.size());
    }

    Basic_string& replace(const_iterator first, const_iterator last,
                          const_iterator src_first, const_iterator src_last) {
        return replace(first, last, src_first, src_last-src_first);
    }
    Basic_string& replace(const_iterator first, const_iterator last,
                          iterator src_first, iterator src_last) {
        return replace(first, last, src_first, src_last-src_first);
    }

    template<typename Input_iterator>
    std::enable_if_t<!std::is_integral<Input_iterator>::value,Basic_string&>
    replace(const_iterator first, const_iterator last, Input_iterator src_first, Input_iterator src_last) {
        // strong exception guaranty, iterator may throw
        Basic_string tmp(first,last);
        return replace(first, last, tmp);
    }

    Basic_string& insert(size_type pos, const value_type* cstr, size_type str_size){
        return replace(pos,0,cstr,str_size);
    }
    Basic_string& insert(size_type pos, const value_type* cstr){
        return insert(pos,cstr,traits_type::length(cstr));
    }
    Basic_string& insert(size_type pos, const Basic_string& str){
        return replace(pos,0,str);
    }
    Basic_string& insert(size_type pos,size_type count,value_type ch){
        return replace(pos,0,count,ch);
    }

    Basic_string& append(const value_type* cstr, size_type str_size){
        return insert(size(),cstr,str_size);
    }
    Basic_string& append(const value_type* cstr){
        return append(cstr,traits_type::length(cstr));
    }
    Basic_string& append(const Basic_string& str){
        return insert(size(),str);
    }

    Basic_string& operator +=(const Basic_string& str){
        return append(str);
    }
    Basic_string& operator +=(const value_type* cstr){
        return append(cstr);
    }
    Basic_string& operator +=(value_type c){
        push_back(c);
        return *this;
    }

    Basic_string& erase(size_type pos, size_type count){
        return replace(pos,count,"",0);
    }

    void clear(){
        if(!empty()){
            size(0);
        }
    }

    void push_back(value_type v){
        size_type ns = size()+1;
        if(ns > capacity()){
            capacity(ns);
        }
        traits_type::assign(get_data()[ns-1],v);
        size(ns);
    }

    value_type*         data()       noexcept   {return get_data();}
    const value_type*   data() const noexcept   {return get_data();}
    const value_type*  c_str() const noexcept   {return get_data();}

private:

    using Storage::get_data;

    /// moves data from from_pos to to_pos, changing capacity and size
    ///
    /// reason: an auxiliary method for replace
    void shift(size_type from_pos, size_type to_pos){
        if(from_pos==to_pos) return;
        const size_type sz = size();

        if(sz-from_pos+to_pos > capacity()){
            capacity(sz-from_pos+to_pos);
        }
        if(from_pos<sz){
            traits_type::move(get_data()+to_pos,get_data()+from_pos,sz-from_pos);
        }
        size(sz-from_pos+to_pos);
    }

    void validate_range(size_type pos, size_type& count)const{
        const size_type sz = size();
        // I don't like to throw, but std::string does.
        // so lets throw to follow this excepected behaviour.
        if(pos>sz){
            throw std::range_error("the given position is greater than size");
        }
        if(count > sz-pos){
            count = sz-pos;
        }
    }

    // no checking
    void size(size_type new_size){
        assert(new_size<=capacity());
        Storage::set_size(new_size);
        traits_type::assign(get_data()[new_size],value_type());
    }
    void capacity(size_type new_capacity){
        // std::string behaviour
        if(new_capacity>max_size()){
            throw std::length_error("resulting capacity is greater than max_size()");
        }
        // exponental grow required
        size_type ccap = capacity();
        if(new_capacity > ccap && new_capacity < ccap*2){
            new_capacity = ccap*2;
        }
        Storage::set_capacity(new_capacity);
    }

    template<typename Input_iterator>
    void fill_with(Input_iterator first, Input_iterator last, std::forward_iterator_tag){
        size_type sz = std::distance(first,last);
        capacity(sz);
        value_type* dst = get_data();
        for(;first!=last;(void)++first,++dst){
            traits_type::assign(*dst,*first);
        }
        size(sz);
    }
    template<typename Input_iterator>
    void fill_with(Input_iterator first, Input_iterator last, std::input_iterator_tag){
        size_type sz = 0;
        size_type cap = capacity();
        while(first!=last){
            if(sz==cap){
                size(sz);
                // note: capacity grows more than on 1
                capacity(sz+1);
                cap = capacity();
            }
            traits_type::assign(get_data()[sz],*first);
            ++sz;
            ++first;
        }
        size(sz);
    }

    using Alloc_traits = std::allocator_traits<allocator_type>;
};

/*
 * some optimized overloads.
 * should be out of the class to prevent possible implicit conversions of non-string argument to const Ch*.
 * operator+(const Basic_string& str, const Ch* cstr) is done via
 * operator+(const Basic_string& str, Basic_string&& str)
*/
template<typename Ch,typename Tr,typename Allc, template<typename,typename,typename> class Sp>
inline task::Basic_string<Ch,Tr,Allc,Sp> operator+(task::Basic_string<Ch,Tr,Allc,Sp>&& str, const Ch* cstr){
    return std::move(str.append(cstr));
}
template<typename Ch,typename Tr,typename Allc, template<typename,typename,typename> class Sp>
inline task::Basic_string<Ch,Tr,Allc,Sp> operator+(const Ch* cstr, task::Basic_string<Ch,Tr,Allc,Sp>&& str){
    return std::move(str.insert(0,cstr));
}

template<typename Ch,typename Tr,typename Allc, template<typename,typename,typename> class Sp>
inline task::Basic_string<Ch,Tr,Allc,Sp> operator+(task::Basic_string<Ch,Tr,Allc,Sp>&& str, Ch c){
    return std::move(str+=c);
}
template<typename Ch,typename Tr,typename Allc, template<typename,typename,typename> class Sp>
inline task::Basic_string<Ch,Tr,Allc,Sp> operator+ (const task::Basic_string<Ch,Tr,Allc,Sp>& str, Ch c) {
    // let NRVO work
    // std::move(Basic_string<Ch,Tr,Allc,Sp>(str)+=c) still
    // involves redundant move construction (at least on gcc)
    Basic_string<Ch,Tr,Allc,Sp> tmp(str);
    tmp+=c;
    return tmp;
}
template<typename Ch,typename Tr,typename Allc, template<typename,typename,typename> class Sp>
inline task::Basic_string<Ch,Tr,Allc,Sp> operator+(Ch c, task::Basic_string<Ch,Tr,Allc,Sp>&& str){
    return std::move(str.insert(0,1,c));
}
template<typename Ch,typename Tr,typename Allc, template<typename,typename,typename> class Sp>
inline task::Basic_string<Ch,Tr,Allc,Sp> operator+(Ch c, const task::Basic_string<Ch,Tr,Allc,Sp>& str){
    Basic_string<Ch,Tr,Allc,Sp> tmp(str);
    tmp.insert(0,1,c);
    return tmp;
}

// basic comparison with c-string, the rest is generated by Comparable
template<typename Ch,typename Tr,typename Allc, template<typename,typename,typename> class Sp>
inline bool operator == (const Basic_string<Ch,Tr,Allc,Sp>& lhv, const Ch* rhv) {
    if(!rhv) return false;
    if(lhv.size()!=Tr::length(rhv)) return false;
    return !Tr::compare(lhv.data(),rhv,lhv.size());
}

template<typename Ch,typename Tr,typename Allc, template<typename,typename,typename> class Sp>
inline bool operator < (const Basic_string<Ch,Tr,Allc,Sp>& lhv, const Ch* rhv) {
    if(!rhv) return false;
    return lhv.compare(rhv)<0;
}

template<typename Ch,typename Tr,typename Allc, template<typename,typename,typename> class Sp>
inline bool operator < (const Ch* lhv, const Basic_string<Ch,Tr,Allc,Sp>& rhv) {
    if(!lhv) return false;
    return rhv.compare(lhv)>0;
}

template<
        typename Char_tp,
        typename Char_traits_tp = std::char_traits<Char_tp>,
        typename Allocator_tp = std::allocator<Char_tp>,
        template<typename,typename,typename> class Storage_policy_tp
        >
inline std::basic_ostream<Char_tp,Char_traits_tp>& operator<<(
        std::basic_ostream<Char_tp,Char_traits_tp>& ostr,
        const task::Basic_string<Char_tp,Char_traits_tp,Allocator_tp,
        Storage_policy_tp>& str) {
    ostr.write(str.data(),str.size());
    ostr.flush();
    return ostr;
}

using String = Basic_string<char,std::char_traits<char>,std::allocator<char>>;

}
#include "str.inl"
